# unicode - A basic unicode listing tool

## Build

To build, a C compiler like GCC or Clang is needed. It can simply be compiled
with

```
gcc -O2 -o unicode unicode.c    # for GCC
clang -O2 -o unicode unicode.c  # for Clang
```

Once installed, the UCD needs to be installed locally on your system. The UCD
can be found here: https://www.unicode.org/Public/UCD/latest/ucd/
Download `UCD.zip` and either place the content

* in `/usr/local/share/unicode` (you need to create the `unicode` directory if
  it doesn't exist); or
* anywhere else, but don't forget you have pass `-D /path/to/UnicodeData.txt`
  for every invocation.

Once this is done, test it by running `./unicode 20`, and you should get:

```
U+0020: SPACE
  Category: Separator, Space
  Text direction: Whitespace
```

If you get the message `Unable to open UCD file`, that means that the `unicode`
directory is not placed in the correct path or that you passed the wrong path to
`-D`. Check the paths and then try again.

## Install

To install globally, run the following commands:

```
sudo cp unicode /usr/local/bin/unicode
sudo chown root:root /usr/local/bin/unicode
sudo chmod 755 /usr/local/bin/unicode
```

Note that it's strongly recommended that you place the UCD in the global
directory if you plan to install globally, so everyone on the system can use it
without any additional work.
